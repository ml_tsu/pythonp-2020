from tracer import FileTracer
import argparse


parser = argparse.ArgumentParser(
    description="Сравнить слепки"
)

parser.add_argument("--old", type=str, required=True,
                    help="Старый слепок")

parser.add_argument("--new", type=str, required=True,
                    help="Новый слепок")


args = parser.parse_args()




try:
    old_tracer = FileTracer()
    old_tracer.load(args.old)
    
    new_tracer = FileTracer()
    new_tracer.load(args.new)
except FileNotFoundError as e:
    print(e)
    exit(1)
    
result = old_tracer.diff(new_tracer)

created_files   = [f'[CREATED][FILE]: {file}' for file in result["created_files"]]
deleted_files   = [f'[DELETED][FILE]: {file}' for file in result["deleted_files"]]

created_folders = [f'[CREATED][DIR ]: {folder}' for folder in result["created_folders"]]
deleted_folders = [f'[DELETED][DIR ]: {folder}' for folder in result["deleted_folders"]]
    
result = created_files + deleted_files + created_folders + deleted_folders

for row in sorted(result, key=lambda x: x[15:]):
    print(row)
    
print("DONE")