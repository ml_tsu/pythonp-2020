from tracer import FileTracer
import argparse


parser = argparse.ArgumentParser(
    description="Создать слепок директории"
)

parser.add_argument("-i", "--input", type=str, required=True,
                    help="Директория для сканирования")

parser.add_argument("-o", "--output", type=str, required=True,
                    help="Файл, куда будет записан слепок")


args = parser.parse_args()

tracer = FileTracer()

try:
    tracer.scan(path=args.input)
except FileNotFoundError as e:
    print(e)
    exit(1)
    
try:
    tracer.save(args.output)
except Exception as e:
    print(e)
    exit(1)
    
print("DONE")