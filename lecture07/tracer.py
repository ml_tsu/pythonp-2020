import os.path
import os


class FileTracer:
    """
        Позволяет делать слепки директорий и сравнивать их друг
        с другом, чтобы можно было определить изменения в этих директориях
    """
    
    def __init__(self):
        self._snapshot = None
        self._path = None
    
    
    def _scan(self, path):
        result = {
            "files": {},
            "folders": {},
        }
        
        for name in os.listdir(path):
            fullpath = os.path.join(path, name)

            if os.path.isfile(fullpath):
                result["files"][fullpath] = {}
            elif os.path.isdir(fullpath):
                result["folders"][fullpath] = {}
                
        for folder in result["folders"]:
            result["folders"][folder] = self._scan(folder)
            
        return result
        
    
    def scan(self, path: str):
        self._path = os.path.abspath(path)
        print(self._path)
        self._snapshot = self._scan(self._path)
        
        
    def _diff(self, old, new):
        result = {
            "deleted_files": set(),
            "created_files": set(),
            
            "deleted_folders": set(),
            "created_folders": set(),
        }
        created_folders = set()
        deleted_folders = set()
        same_folders = set()
        
        if old is None and new is None:
            return result
        elif old is not None and new is None:
            result["deleted_files"] = set(old["files"])
            result["deleted_folders"] = set(old["folders"])
        elif old is None and new is not None:
            result["created_files"] = set(new["files"])
            result["created_folders"] = set(new["folders"])
        else:
            s_old = set(old["files"])
            s_new = set(new["files"])
            result["deleted_files"] = s_old - s_new
            result["created_files"] = s_new - s_old

            s_old = set(old["folders"])
            s_new = set(new["folders"])
            deleted_folders = s_old - s_new
            same_folders = s_old & s_new
            created_folders = s_new - s_old

            result["deleted_folders"] = deleted_folders
            result["created_folders"] = created_folders
        
        for folder in deleted_folders:
            tmp = self._diff(old["folders"][folder], None)
            for k in result:
                result[k] |= tmp[k]
        for folder in created_folders:
            tmp = self._diff(None, new["folders"][folder])
            for k in result:
                result[k] |= tmp[k]
        for folder in same_folders:
            tmp = self._diff(old["folders"][folder], new["folders"][folder])
            for k in result:
                result[k] |= tmp[k]
                
        return result
        
    
    def diff(self, other):
        if self._path != other._path:
            raise Exception("Различные директории")
            
        return self._diff(self._snapshot, other._snapshot)
    
    
    
    def save(self, filepath: str):
        with open(filepath, "wb") as f:
            import pickle
            pickle.dump(self, f)
    
    def load(self, filepath: str):
        with open(filepath, "rb") as f:
            import pickle
            
            tmp = pickle.load(f)

            self._path = tmp._path
            self._snapshot = tmp._snapshot
            
    
    

if __name__ == "__main__":
    tracer = FileTracer()
    tracer.scan(".")
    
    print("DONE")
    